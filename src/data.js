const data = {
    products:[
        {
            id: '1',
            name:'Macbook',
            price: 1400,
            image: 'https://www.trustedreviews.com/wp-content/uploads/sites/54/2021/10/Macbook-Pro-2021-13-1.png'
        },
        {
            id: '2',
            name:'Apple Watch',
            price: 400,
            image: 'https://www.trustedreviews.com/wp-content/uploads/sites/54/2022/09/Apple-Watch-Ultra-2.png'
        },
        {
            id: '3',
            name:'iPhone',
            price: 900,
            image: 'https://pixahive.com/wp-content/uploads/2020/11/iPhone-SE-193795-pixahive.jpg'
        }
    ]
};
export default data;